class Color:
    def __init__(self, red, green, blue):
        self.red = red
        self.green = green
        self.blue = blue

    def __str__(self):
        return "RGB: %f, %f, %f" % (float(self.red), float(self.green), float(self.blue))

