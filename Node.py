from Color import Color


class Node:
    def __init__(self, biomorph, level,
                 create_successors=False, ancestor=None, edge_len=0, left_node=None, right_node=None):
        self.biomorph = biomorph
        self.level = level
        self.edge_len = edge_len
        self.ancestor = ancestor
        self.left_node = left_node
        self.right_node = right_node

        self.create_successors()

    def __str__(self):
        return "Node level %d" % self.level

    def create_successors(self):
        if self.level == self.biomorph.depth:    # Is last level
            return

        next_level = self.level + 1
        edge_len = self.get_edge_length()
        self.left_node = self.create_single_successor(next_level, edge_len)
        self.right_node = self.create_single_successor(next_level, edge_len)

    def create_single_successor(self, level, edge_len):
        return Node(self.biomorph, level, self, edge_len)

    def get_edge_length(self):
        edge_len = 0
        if self.level % 2 == 0:                 # For even level
            edge_len = self.biomorph.even_edge_len
        else:                                   # For odd level
            edge_len = self.biomorph.odd_edge_len

        return self.biomorph.len_multiplier * edge_len

    def get_segment_color(self):

        # TODO Add validation

        color = Color(self.biomorph.red, self.biomorph.green, self.biomorph.blue)
        for i in range(0, self.level):
            color.red *= self.biomorph.gradient
            color.blue *= self.biomorph.gradient
            color.green *= self.biomorph.gradient
        return color