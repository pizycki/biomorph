from Biomorph import Biomorph
from Math import Math
from Direction import Direction
from Lgr import Lgr


class BiomorphDrawer(Biomorph):
    turtle = None

    def __init__(self, biomorph):
        self.biomorph = biomorph

    def draw(self, turtle):
        self.turtle = turtle

        # Draw hole biomorph
        self.draw_branches(self.biomorph.root)

    def draw_branches(self, node):
        Lgr.println("draw_branches : level %d : START" % node.level)

        if node.left_node is not None:
            self.draw_branch(node, Direction.left)

        if node.right_node is not None:
            self.draw_branch(node, Direction.right)

        Lgr.println("draw_branches: level % d: END" % node.level)

    def draw_branch(self, node, direction):
        Lgr.println("draw_branch : level %d : direction: %d : START" % (node.level, direction))

        # Rotate turtle
        angle = self.get_angle(node)
        self.turn_turtle(direction, angle)

        # Set color
        self.set_turtle_color(node)

        # Draw edge
        lentgh = self.get_edge_length(node)
        self.turtle.forward(lentgh)

        # Continue cascade
        # self.turn_turtle(-direction, angle)  # Face north!
        successor = node.left_node if direction == Direction.left else node.right_node
        self.draw_branches(successor)
        # self.turn_turtle(direction, angle)

        # Go back to start point
        self.turtle.penup()
        self.turtle.back(lentgh)
        self.turn_turtle(-direction, angle)
        self.turtle.pendown()

        Lgr.println("draw_branch : level %d : direction: %d : END" % (node.level, direction))

    def get_angle(self, node):
        return self.biomorph.even_angle if Math.is_even(node.level + 1) else self.biomorph.odd_angle

    def get_edge_length(self, node):
        edge_len = self.biomorph.even_edge_len if Math.is_even(node.level + 1) else self.biomorph.odd_edge_len
        return edge_len * self.biomorph.len_multiplier

    def turn_turtle(self, direction, angle):
        if direction == Direction.left:
            self.turtle.left(angle)
        elif direction == Direction.right:
            self.turtle.right(angle)
        else:
            pass

    def set_turtle_color(self, node):
        c = node.get_segment_color()
        self.turtle.pencolor(c.red, c.green, c.blue)