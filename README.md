Biomorfy to cyfrowe stworzenia, które kształtem mogą przypominać obiekty czy zwierzęta. Biomorfy są strukturami drzewiastymi (binarnymi), tzn. biomorf ma stawy (węzły) i segmenty (krawędzie), do każdego stawu poza zerowym (czyli poza korzeniem) wchodzi jeden segment, a wychodzą z niego dwa.

Każdy biomorf ma genotyp, czyli listę 10 genów określających jego wygląd. Geny mają różny wpływ na biomorfa i są reprezentowane przez liczby całkowite albo rzeczywiste.

## Opis genów ##

* gen 0 - głębokość, czyli liczba rozgałęzień biomorfa
* geny 1 i 2 - kąty odchylenia kolejnych segmentów od osi poprzedniego segmentu (w stopniach). Gen 1 odpowiada odchyleniom na rozgałęzieniach o nieparzystej głębokości, a gen 2 parzystej.
* geny 3 i 4 - długości segmentów odpowiednio na nieparzystej (gen3) i parzystej (gen4) głębokości (wyrażona w pikselach)
* gen 5 - wydłużenie/skrócenie segmentów.  Parzyste (nieparzyste) segmenty stopnia i są długości gen4*gen5**i (gen3*gen5**i) poprzedniego parzystego (nieparzystego)
* gen 6,7,8 - geny kodujące kolor biomorfa (w notacji RGB), z zakresu 0.01-0.99
* gen 9 - gradient koloru (czyli rozjaśnienie/przyciemnienie segmentów na kolejnych głębokościach). Tak jak wydłużenie, jest to cecha multiplikatywna

Biomorfy są jednopłciowe, a ich kolejne pokolenia różnią się
od poprzedniego poprzez losową, pojedynczą mutacje - drobną zmianę jednego
z ich genów.

## Opis mutacji ##

* gen 0 - głębokość zmienia się o +/-1
* gen 1 i 2 - odchylenie zmienia się o +/-10 stopni
* gen 3 i 4 - długość zmienia się o +/- 10 pikseli
* gen 5 - przeskalowanie o losową wartość z przedziału [0.75, 1.5]
* gen 6, 7 i 8 - przeskalowanie o losową wartość z przedziału [0.75, 1.5] (wynik skalowania spoza przedziału [0.01-0.99] powinien otrzymać wartość najbliższego krańca przedziału)
* gen 9 -przeskalowanie o losową wartość z przedziału [0.75, 1.5]


### Przykład 1: ###
Biomorf o genotypie [2, 45, 10, 50, 20, 1, 0.1, 0.1, 0.1, 2 ] , wraz z mutantami.

### Przykład 2: ###
Biomorf o genotypie [4, 120, 30, 60, 20, 1.5, 0.2, 0.2, 0.4, 1.5] wraz z mutantami

Napisz pakiet biomorph zawierający 3 moduły do symulowania ewolucji biomorfów.

* (3pkt) Moduł mutacja będzie generował zmutowane biomorfy. Powinien
zawierać funkcję mutuj(biomorf), która zwróci listę 8 biomorfów
pochodzących z losowych mutacji wejściowego biomorfa.
* (7pkt + 3pkt) moduł rysowanie powinien zawierać funkcję
rysuj(biomorf) rysującą przy użyciu modułu turtle danego biomorfa (5 pkt),
oraz funkcję rysuj_wszystkie(biomorf, listamutantów), która narysuje na środku wejściowego biomorfa oraz wokół wszystkie jego 8 potomnych zmutowanych biomorfów w formie obrazka 3x3 (patrz przykład 2).
* (7pkt) moduł symulacja: pozwoli na symulowanie ewolucji biomorfa
korzystając z doboru (który zapewnia użytkownik) :
    * (4 pkt) Moduł powinien zawierać funkcję symuluj(startowy), która startując od zadanego biomorfa startowy wyświetla go oraz 8 jego losowych mutantów, a następnie pozwala
    wybrać jeden z nich (wpisując numer od 1 do 9 w konsoli, biomorfy numerujemy rzędami, a w rzędzie od lewej do prawej) lub zakończyć
    symulację wpisując 0 (patrz podpowiedź 1). Funkcja zwraca historię symulacji, czyli listę zawierającą kolejne pary: (lista mutantów, numer wybranego mutanta).
    * (3 pkt) Moduł powinien oferować możliwość uruchamiania jako skrypt z parametrami:
    -p plik - nazwa pliku z opisem biomorfa (jeden wiersz-jeden gen)
    -o output - (opcjonalny) nazwa pliku do którego należy wypisać historię symulacji (jeden wiersz - jedna para (lista mutantów, numer wybranego mutanta))
    -b gen0 gen1 ... gen9 - startowy biomorf zapisany jak wartości poszczególnych genów

Do rysowania należy użyć modułu turtle

Podpowiedź 1: Wczytywanie z konsoli: funkcja raw_input("Napis wyświetlany w konsoli") zwracająca napis będący linią wpisaną przez użytkownika.