class BiomorphFile:
    def __init__(self):
        pass

    # Returns file content as list of lines.
    @staticmethod
    def load_biomorph(path):
        f = open(path, "r")

        for line in f:
            yield line.strip()

        f.close()

    @staticmethod
    def log_simulation(path, collection, prefix=None):
        f = open(path, "a+")  # append and read

        separator = ", "
        list_of_biomorphs_genoms = []
        for item in collection:
            list_of_biomorphs_genoms.append(item.get_genoms_as_string(separator))

        txt = "{ %s }" % separator.join(list_of_biomorphs_genoms)

        if prefix is not None:
            txt = prefix + txt

        f.write(txt)
        f.close()

    @staticmethod
    def create_file(path):
        f = open(path, 'w+')    # TODO check is erasing file
        f.close()

class Cmd:
    def __init__(self):
        pass

    @staticmethod
    def _get_single_arg_value(arg_name, args):
        idx = 0
        while idx < len(args):
            if args[idx] == arg_name:
                return args[idx + 1]
            idx += 1

    @staticmethod
    def get_source_biomorph_file(args):
        arg_name = "-p"
        return Cmd._get_single_arg_value(arg_name, args)

    @staticmethod
    def get_sim_log_file_path(args):
        arg_name = "-o"
        return Cmd._get_single_arg_value(arg_name, args)

    @staticmethod
    def get_biomorph_from_args(args):
        arg_name = "-b"
        idx = 0
        counter = 0
        genoms = []
        recording = False
        while idx < len(args):
            if args[idx] == arg_name:
                recording = True
            elif recording and counter < 10:
                counter += 1
                genoms.append(args[idx])
            idx += 1
        return genoms


