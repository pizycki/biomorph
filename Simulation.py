from IO import *
from SimulationWindow import *
from MutantGenerator import *


class Simulation:

    biomorphs = []
    simWindow = None
    log_path = None

    def __init__(self):
        pass

    def simulate(self, biomorph, log_path=None):
        self.biomorphs = Simulation._get_biomorph_and_mutants(biomorph)

        self.simWindow = SimulationWindow(10)   # 1 .. 10
        self.simWindow.draw_all_biomoprhs(self.biomorphs)
        self.log_path = log_path


    def pick(self, idx):
        if self.biomorphs is None or len(self.biomorphs) == 0:
            pass

        # Log simulation step to file
        if self.log_path is not None:
            message = "Wybrany biomorf: %d, lista wsyzstkich biomorfow: " % idx
            BiomorphFile.log_simulation(self.log_path, self.biomorphs, message)

        picked_biomorph = self.biomorphs[idx-1]
        self.simulate(picked_biomorph)

    @staticmethod
    def _get_biomorph_and_mutants(biomorph):
        # TODO Change to insert in the center of list
        items = [biomorph] + Simulation._generate_mutants(biomorph)
        return items

    @staticmethod
    def _generate_mutants(biomorph):
        generator = MutantGenerator()
        mutants = generator.mutate(biomorph, 8)     # We want eight mutants
        return mutants