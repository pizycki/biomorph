from IO import *
from Biomorph import *


path = "content/test_log.txt"
biomorhs = [
    Biomorph(4, 120, 30, 30, 10, 1.5, .75, .3, .4, 0.9),
    Biomorph(2, 320, 50, 10, 70, 1.5, .75, .3, .2, 0.3)
]

prefix = "TEst prefi: "

BiomorphFile.log_simulation(path, biomorhs, prefix)