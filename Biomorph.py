from Node import Node


class Biomorph:

    root = None

    def __init__(self, depth, odd_angle, even_angle, odd_edge_len, even_edge_len, len_multiplier, red, green, blue,
                 gradient, build=True):

        self.depth = int(depth)
        self.even_angle = float(even_angle)
        self.odd_angle = float(odd_angle)
        self.even_edge_len = float(even_edge_len)
        self.odd_edge_len = float(odd_edge_len)
        self.len_multiplier = float(len_multiplier)
        self.red = float(red)
        self.green = float(green)
        self.blue = float(blue)
        self.gradient = float(gradient)

        self.build()

    # By default, creating first node will initiate chain reaction and all nodes will be created
    def build(self):
        self.root = Node(self, 0)
    
    def clone(self, build=False):
        return Biomorph(self.depth, self.odd_angle, self.even_angle, self.odd_edge_len, self.even_edge_len,
                        self.len_multiplier, self.red, self.green, self.blue, self.gradient, build)

    def __str__(self):
        return \
            "self.depth: " + str(self.depth) + "\n"\
            + "self.even_angle: " + str(self.even_angle) + "\n"\
            + "self.odd_angle: " + str(self.odd_angle) + "\n"\
            + "self.odd_edge_len: " + str(self.odd_edge_len) + "\n"\
            + "self.even_edge_len: " + str(self.even_edge_len) + "\n"\
            + "self.len_multiplier: " + str(self.len_multiplier) + "\n"\
            + "self.red: " + str(self.red) + "\n"\
            + "self.green: " + str(self.green) + "\n"\
            + "self.blue: " + str(self.blue) + "\n"\
            + "self.gradient: " + str(self.gradient) + "\n"

    @staticmethod
    def create_biomorph(genoms):
        return Biomorph(genoms[0], genoms[1], genoms[2],
                        genoms[3], genoms[4], genoms[5],
                        genoms[6], genoms[7], genoms[8],
                        genoms[9])

    # Hash code for object. Used to compare.
    def get_hash(self):
        return self.__str__()

    # Returns list of genoms describing biomorph
    def get_genoms(self):
        return [self.depth, self.odd_angle, self.even_angle, self.odd_edge_len, self.even_edge_len,
                self.len_multiplier, self.red, self.green, self.blue, self.gradient]

    def get_genoms_as_string(self, separator):
        genoms_as_strings = []
        for g in self.get_genoms():
            genoms_as_strings.append(str(g))
        return "[ %s ]" % separator.join(genoms_as_strings)