import turtle
from BiomorphDrawer import *
from Math import Point


class SimulationWindow:

    def __init__(self, speed):
        self.window = turtle.Screen()
        self.speed = speed
        self.frank = SimulationWindow._get_frank(self.speed)

    def draw_all_biomoprhs(self, biomorphs):
        # TODO: Get actual window size. For now, it's turtle module window default.
        # turtle.window_width()
        # turtle.window_height()

        width = 960
        height = 810
        n = len(biomorphs)

        # Calculate root points where biomorphs will be drawn
        root_points = SimulationWindow._get_root_points(n)

        # Go go, Super Turtle!
        self._reset_frank()

        # Draw all biomorphs
        for idx in range(0, len(biomorphs)):
            p = root_points[idx]
            b = biomorphs[idx]
            SimulationWindow.draw_biomorph(self.frank, b, p)

        self.window.exitonclick()

    @staticmethod
    def draw_biomorph(frank, biomorph, start_point):
            # Move frank to starting point
            SimulationWindow._move_frank(frank, start_point)
            drawer = BiomorphDrawer(biomorph)
            drawer.draw(frank)

    @staticmethod
    def _move_frank(frank, p):
        frank.penup()
        frank.goto(p.x, p.y)
        frank.pendown()

    @staticmethod
    def _get_frank(speed):
        return turtle.Turtle()

    def _reset_frank(self):
        if self.frank is None:
            self.frank = SimulationWindow._get_frank(self.speed)

        self.frank.reset()
        self.frank.clear()
        self.frank.color(0, 0, 0)    # Set turtle colour to black (default)
        self.frank.pensize(3)        # Set turtle brush width
        self.frank.left(90)    # Turtle starts with face turned to North

    @staticmethod
    def _get_avgx(width, points):
        width = float(width)
        avg_x = width / points
        return avg_x / 2

    @staticmethod
    def _get_avgy(height, points):
        height = float(height)
        avg_y = height / points
        return avg_y

    @staticmethod
    def _get_root_points(n):
        return [
            # First row
            Point(-320, 185)
            , Point(0, 185)
            , Point(320, 185)

            # Second row
            , Point(-320, 0)
            , Point(0, 0)
            , Point(320, 0)

            # Third row
            , Point(-320, -185)
            , Point(0, -185)
            , Point(320, -185)
        ]