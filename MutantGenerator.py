import random

from Math import Math


class MutantGenerator:
    def __init__(self):
        # Avaible mutations, note that those are methods!
        self.mutations = [
            MutantGenerator._mutate_depth,
            MutantGenerator._mutate_even_angle,
            MutantGenerator._mutate_odd_angle,
            MutantGenerator._mutate_even_length,
            MutantGenerator._mutate_odd_length,
            MutantGenerator._mutate_edge_len_multiplier,
            MutantGenerator._mutate_red,
            MutantGenerator._mutate_green,
            MutantGenerator._mutate_blue,
            MutantGenerator._mutate_gradient
        ]

    # Returns list of clones with random type of mutation
    def mutate(self, biomorph, number):
        mutants = []
        for i in range(0, number):
            mutant = MutantGenerator._generate_unique_mutant(biomorph, self.mutations, mutants)
            mutants.append(mutant)
            
        return mutants
                
    @staticmethod
    def _generate_unique_mutant(biomorph, mutations, generetated_mutants):
        unique = False
        clone = None
        while not unique:
            clone = biomorph.clone()
            mutate = list(MutantGenerator._get_rand_mutations(mutations, 1))[0]   # generate one mutations
            mutate(clone)    # Mutate! (contains side effects)
            unique = MutantGenerator._is_unique_within_list(clone, generetated_mutants)

        clone.build()

        return clone

    @staticmethod
    def _is_unique_within_list(mutant, mutants_list):
        for m in mutants_list:
            if m.get_hash() == mutant.get_hash():
                return False
        return True

    @staticmethod
    def _mutate_depth(biom):
        new_depth = MutantGenerator._rand_factor() + biom.depth     # levels
        biom.depth = new_depth

    @staticmethod
    def _mutate_odd_angle(biom):
        angle_change = MutantGenerator._rand_factor() * 10  # angles
        biom.odd_angle += angle_change

    @staticmethod
    def _mutate_even_angle(biom):
        angle_change = MutantGenerator._rand_factor() * 10  # angles
        biom.odd_angle += angle_change

    @staticmethod
    def _mutate_even_length(biom):
        len_change = MutantGenerator._rand_factor() * 10     # pixels
        biom.even_edge_len += len_change

    @staticmethod
    def _mutate_odd_length(biom):
        len_change = MutantGenerator._rand_factor() * 10     # pixels
        biom.odd_edge_len += len_change

    @staticmethod
    def _mutate_edge_len_multiplier(biom):
        len_mult = biom.len_multiplier + random.uniform(0.75, 1.5)
        if len_mult < 0:
            len_mult = 0.01
        biom.len_multiplier = len_mult

    @staticmethod
    def _mutate_red(biom):
        biom.red = MutantGenerator._get_random_hue(biom.red)

    @staticmethod
    def _mutate_green(biom):
        biom.green = MutantGenerator._get_random_hue(biom.green)

    @staticmethod
    def _mutate_blue(biom):
        biom.blue = MutantGenerator._get_random_hue(biom.blue)

    @staticmethod
    def _get_random_hue(hue):
        hue_diff = random.uniform(0.75, 1.5)
        factor = MutantGenerator._rand_factor()
        hue += factor * hue_diff

        if hue > 0.99:
            hue = 0.99
        elif hue < 0.01:
            hue = 0.01

        return hue

    @staticmethod
    def _mutate_gradient(biom):
        factor = MutantGenerator._rand_factor()
        gradient = biom.gradient + (factor * random.uniform(0.75, 1.5))     # TODO for better results, narrow range

        if gradient > 0.99:
            gradient = 0.99
        elif gradient < 0.01:
            gradient = 0.01

        biom.gradient = gradient

    # Draw 1 or -1
    @staticmethod
    def _rand_factor():
        d = random.randint(1, 2)
        return 1 if Math.is_even(d) else -1

    @staticmethod
    def _get_rand_mutations(mutations, number):
        avaible_mutations_count = len(mutations)
        idxs = Math.get_rand_numbers(0, avaible_mutations_count - 1, number)
        for idx in idxs:
            yield mutations[idx]