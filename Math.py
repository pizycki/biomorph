import random


class Math:

    def __init__(self):
        pass

    @staticmethod
    def is_even(number):
        return number % 2 == 0

    # Returns list of pseudo-random generated numbers
    @staticmethod
    def get_rand_numbers(start, end, n):
        for i in range(0, n):
            yield random.randint(start, end)


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y