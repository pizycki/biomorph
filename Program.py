from Simulation import *
from Biomorph import Biomorph
from IO import *
import sys

# Program

# Try to load biomorph from file
source_file = Cmd.get_source_biomorph_file(sys.argv)
if source_file is not None:
    genoms = list(BiomorphFile.load_biomorph(source_file))
    biomorph = Biomorph.create_biomorph(genoms)

# Try to load biomorph from args
else:
    genoms = Cmd.get_biomorph_from_args(sys.argv)
    biomorph = Biomorph.create_biomorph(genoms)

# Check is biomorph loaded
if biomorph is None:
    sys.exit("Init biomorph not found!")

# (Optional) Retrieve simulation log file path from args
sim_log_path = Cmd.get_sim_log_file_path(sys.argv)

# biomorph2 = Biomorph(4, 120, 30, 30, 10, 1.5, .75, .3, .4, 0.9)

sim = Simulation()
sim.simulate(biomorph, sim_log_path)

key = -1
while key != 0:
    print "Pick index of biomorph to continue simulation."
    key = int(raw_input())
    print "You picked %d" % key

    if key == 0:
        break

    if key > 0 or key < 10:
        sim.pick(key)

print "Game Over"